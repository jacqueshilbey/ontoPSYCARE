# OntoPSYCARE

## Archive de l'ontologie modulaire ontoPSYCARE
Cette archive contient deux répertoires : 
- l'un dédié aux différents modules de l'ontologie ontoPSYCARE (ontoPSYCARE_modules) et 
- l'autre à une version fusionnée de tous les modules (ontoPSYCARE_merged_version).

Le schéma ci-dessous explique la structure de l'ontologie ontoPSYCARE (les flèches correspondant au mécanisme d'importation).

```mermaid
graph BT;
  ontoPSYCARE-->ontoDOIMA;
  ontoPSYCARE-->ontoDOBIO;
  ontoPSYCARE-->ontoDODISC;
  ontoPSYCARE-->ontoDODEXT;
  ontoPSYCARE-->ontoMEDPSY;
  ontoPSYCARE-->ontoDOPSY;
  ontoDODEXT-->ontoDOME;
  ontoDODISC-->ontoDOME;
  ontoDOBIO-->ontoDOME;
  ontoDOIMA-->ontoDOME;
  ontoMEDPSY-->ontoDOME;
  ontoDOPSY-->ontoDOME;
  ontoDOME-->ontoPOF;
```

## Contributeurs
Chacune des ontologies indique ses contributeurs.

## Licences
Toutes les ontologies sont sous la licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)

## Statut du projet
L'état d'avancement des ontologies présentées ici correspond au travail accompli à la fin de 2023.

## Remerciements
Ce travail a bénéficié d’une aide de l’Etat gérée par l’Agence Nationale de la Recherche au titre du Programme d’Investissements d’Avenir portant la référence PsyCARE ANR-18-RHUS- 0014.

## Références
- [Site du projet PsyCARE](https://psy-care.fr/)
- Thèse de Jacques Hilbey :
    - [Médecine de précision : la voie ontologique - Application à la psychiatrie](https://hal.science/tel-04300740v1)
- Articles de Jacques Hilbey et Jean Charlet relatifs à ontoPSYCARE : 
    - [Temporal Medical Knowledge Representation Using Ontologies](https://hal.science/hal-03683532v1)
    - [An Ontology Design Pattern for Modeling Experimental Paradigms](https://hal.science/hal-04148022v1)

